function createNewUser() {
    let firstName = prompt('Enter your name: ', 'Rus');
    let lastName = prompt('Enter your surname: ', 'Ste');
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName: function (FirstName) {
            Object.defineProperties(this, {
                firstName: {
                    value: FirstName
                }
            })
        },
        setLastName: function (LastName) {
            Object.defineProperties(this, {
                lastName: {
                    value: LastName
                }
            })
        }
    };
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });
    return newUser;
}

let newUser = createNewUser();

// test

console.log("Create new user: ", newUser);
console.log("Login: " + newUser.getLogin());
newUser.firstName = "Rusl";
console.log("Change name: ", newUser);
newUser.setFirstName('Ruslan');
console.log("Chane name on func. setFirstName: ", newUser);
newUser.setLastName('Stepenko');
console.log("Chane name on func. setLastName: ", newUser);
console.log("Login: " + newUser.getLogin());
