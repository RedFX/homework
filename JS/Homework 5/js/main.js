function createNewUser() {
    let firstName = prompt('Enter your name: ', 'Rus');
    let lastName = prompt('Enter your surname: ', 'Ste');
    let birthday = prompt('Enter birthday date: ', '28.04.1996');
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday.split('.'),
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName: function (FirstName) {
            Object.defineProperties(this, {
                firstName: {
                    value: FirstName
                }
            })
        },
        setLastName: function (LastName) {
            Object.defineProperties(this, {
                lastName: {
                    value: LastName
                }
            })
        },
        getAge: function () {
            return +(new Date(new Date - new Date(+this.birthday[2], +this.birthday[1] - 1, +this.birthday[0])).getFullYear()) - 1970;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday[2];
        },
    };
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });
    return newUser;
}

let newUser = createNewUser();

// test

console.log('Creating user...');
console.log("Created user: ", newUser);
console.log("User age: " + newUser.getAge());
console.log("User password: " + newUser.getPassword());
