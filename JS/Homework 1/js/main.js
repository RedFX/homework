//let ageUser = +prompt('Enter your age:', '23');
//let nameUser = prompt('Enter your name:', 'Ruslan');
let ageUser;
let nameUser;

do {
    ageUser = +prompt('Enter your age:', '23');
} while (typeof(ageUser) != "number" || isNaN(ageUser));

do {
    nameUser = prompt('Enter your name:', 'Ruslan');
} while (nameUser === '');


if (ageUser > 22) {
    document.write('<h1>');
    document.write('Welcome, ' + nameUser);
    document.write('</h1>');
} else if (ageUser >= 18 && ageUser <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        document.write('<h1>');
        document.write('Welcome, ' + nameUser);
        document.write('</h1>');
    } else {
        document.write('<h1>');
        document.write('You are not allowed to visit this website');
        document.write('</h1>');
    }
} else {
    document.write('<h1>');
    document.write('You are not allowed to visit this website');
    document.write('</h1>');
}
