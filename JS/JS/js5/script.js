// let firstName = prompt('Enter your name', 'Nikita');
// let lastName = prompt('Enter your surname', 'Pyresev');
//
// function CreateNewUser(firstName, lastName) {
//     Object.defineProperty(this, '_firstName', {
//         value: firstName,
//         configurable: true,
//     });
//
//     Object.defineProperty(this, '_lastName', {
//         value: lastName,
//         configurable: true
//     });
//     this.birthday = prompt('Enter your birthday date', '29.10.1996');
//
//     this.getLogin = function () {
//         return (firstName[0] + lastName).toLowerCase()
//     };
//
//     this.getAge = function () {
//         let dateStr = this.birthday.split('.').reverse().join('-'); //1996-09-24
//         return Math.floor((Date.now() - Date.parse(dateStr)) / 3.154e+10)
//     };
//
//     this.getPassword = function () {
//         return `${firstName[0].toUpperCase()}${lastName.toLowerCase()}${this.birthday.split('.')[2]}`;
//     };
//
//     this.setFirstName = function (value) {
//         Object.defineProperty(this, '_firstName', {
//             value: value
//         });
//         return value; //for final check in console
//     };
//
//     this.setLastName = function (value) {
//         Object.defineProperty(this, '_lastName', {
//             value: value
//         });
//         return value;
//
//
//     };
// }
//
// let newUser = new CreateNewUser(firstName, lastName);
//
// console.log(newUser);
// console.log(`Age: ${newUser.getAge()}`);
// console.log(`Login: ${newUser.getLogin()}`);
// console.log(`Password: ${newUser.getPassword()}`);

// Почему возврящает объект именно с таким порядком свойств? Как упорядочить?

/* Alter code below this line */

class Thermostat {
    constructor(temp) {
        this.temperature = temp;
    }

    get temp() {
        return  5/9 * (this.temperature - 32)
    }

    set temp(value) {
       return  value * 9.0 / 5 + 32
    }
}
/* Alter code above this line */

