// let num;
// do {
//     num = +prompt('Поиск чисел кратных 5\nВведите число: ')
// } while (isNaN(num) || !Number.isInteger(num));

// if (num > 4){
//     for (let i = 0; i <= num; i += 5){
//         console.log(i)
//     }
// } else console.log('Sorry, no numbers');


let m, n;
do {
    m = +prompt('Поиск простых чисел.\nВведите первое число (m):', m);
    n = +prompt('Поиск простых чисел.\nВведите первое число (n):', n);
} while (isNaN(m) || !Number.isInteger(m) || isNaN(n) || !Number.isInteger(n));

outer: for (let i = m; i <= n; i++) {
    if ((i === 1) || (i % 2 === 0)) {
        continue;
    }
    for (let j = 2; j <= Math.floor(Math.sqrt(i)); j++) {
        if (i % j === 0) {
            continue outer;
        }
    }
    console.log(i);
}


