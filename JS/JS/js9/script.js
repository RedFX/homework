let tabs = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.tabs-content li');
let menu = document.querySelector('.tabs');


menu.addEventListener('click', (event) => {
    for (let i = 0; i < tabs.length; i++) {
        if (event.target === tabs[i]) {
            console.log(event.target);
            showContent(i);
        }
    }
});

function hideContent() {
    for (let i = 0; i < tabsContent.length; i++) {
        tabsContent[i].classList.add('hidden');
        tabs[i].classList.remove('active');
    }
}

function showContent(index) {
    hideContent();
    tabs[index].classList.add('active');
    tabsContent[index].classList.remove('hidden')
}

