
let num;
do {
    num = prompt('factorial!\nEnter your number:');
    console.log(num)
} while (isNaN(num) || num === null || num === '');

alert(`${num}! = ${getFactorial(num)}`);

// Factorial (loop)

// function getFactorial(num) {
//     let factorial = 1;
//     for (let i = num; i > 0; i--){
//         factorial *= i;
//     }
//    return factorial
// }

// Factorial (recursion)

function getFactorial(num) {
    return num < 2 ? num : (num * getFactorial(num - 1));
}
