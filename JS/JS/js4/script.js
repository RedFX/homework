// 'use strict'

let firstName = prompt('Enter your name', 'Nikita');
let lastName = prompt('Enter your surname', 'Pyresev');

function CreateNewUser(firstName, lastName) {

    Object.defineProperty(this, '_firstName', {
        value: firstName,
        configurable: true,
    });

    Object.defineProperty(this, '_lastName', {
        value: lastName,
        configurable: true,
});

    this.getLogin = function () {
        return (firstName[0] + lastName).toLowerCase()
    };

    this.setFirstName = function (value) {
        Object.defineProperty(this, '_firstName', {
            value: value
        });
        return value; //for final check in console
    };

    this.setLastName = function (value) {
        Object.defineProperty(this, '_lastName', {
            value: value
        });
        return value;
    };
}

let newUser = new CreateNewUser(firstName, lastName);


console.log(newUser);
console.log(`Login: ${newUser.getLogin()}`);
console.log(`Setting value for _firstName directly:\n${newUser._firstName = 'John'}`);
console.log(`_firstName after directly setting: (still previous)\n${newUser._firstName}`);
console.log(`Setting value via setFirstName() function:\n${newUser.setFirstName('Peter')}`);
console.log(`Setting value via setLastName() function:\n${newUser.setLastName('Brown')}`);
console.log(`Name after setting values via function:\n${newUser._firstName} ${newUser._lastName}`);


