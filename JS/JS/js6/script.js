let arr = ['hello', 'world', 23, '23', null, {name: 'John'}, undefined];

filterBy = (array, type) => array.filter(((value) => typeof (value) !== type));

// forEach implementation
// function filterBy(array, type){
//     let filteredArr = [];
//     array.forEach(function (item) {
//         if (typeof (item) !== type){
//             filteredArr.push(item)
//         }
//     });
//     return filteredArr;
// }


console.log(filterBy(arr, 'string'));


