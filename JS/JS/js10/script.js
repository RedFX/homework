const icons = document.querySelectorAll('.icon-password');
const passwordInputs = document.querySelectorAll('.input-password');
const submitButton = document.querySelector('button');

icons.forEach((icon, index) => {
    icon.addEventListener('click', (event) => {
        changeIcon(event);
        showPassword(index);
    })
});

submitButton.addEventListener('click', (event) => {
    event.preventDefault()
    comparePasswords();
});

function changeIcon(e) {
    e.target.classList.toggle('fa-eye');
    e.target.classList.toggle('fa-eye-slash');
}

function showPassword(i) {
    if (passwordInputs[i].type === 'password') {
        passwordInputs[i].type = 'text';
    } else passwordInputs[i].type = 'password';
}

function comparePasswords() {
    let password = passwordInputs[0].value;
    let confirmationPassword = passwordInputs[1].value;
    if (password === confirmationPassword) {
        alert('You are welcome!');
    } else showError()
}

function showError() {
    const confirmationField = document.querySelector('.input-wrapper:nth-child(2)');
    let errorMessage = document.createElement('span');
    errorMessage.classList.add('error-message');
    errorMessage.innerText = 'Нужно ввести одинаковые значения';
    confirmationField.insertAdjacentElement('afterend', errorMessage)
}
