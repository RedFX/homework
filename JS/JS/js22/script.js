let container = document.querySelector('.container');
let startButton = document.querySelector('.btn-start');

let drawButton = document.createElement('button');
let formElement = document.createElement('form');
let inputElement = document.createElement('input');

const circleCount = 100;

startButton.addEventListener('click', () => {
    startButton.remove();
    container.append(createForm())
});

drawButton.addEventListener('click', () => {
    const circleDiameter = getValue();
    formElement.remove();
    drawCircles(circleDiameter, circleCount);
    removeCircles()

});

function createForm() {
    formElement.classList.add('form');
    inputElement.classList.add('form-input');
    inputElement.setAttribute('placeholder', 'Диаметр круга в пикселях');
    drawButton.classList.add('btn', 'btn-form');
    drawButton.innerText = 'Нарисовать';

    formElement.append(inputElement, drawButton);
    return formElement
}

function getValue() {
    return inputElement.value;
}

function generateCircle(diameter) {
    let circle = document.createElement('div');
    circle.classList.add('circle');
    circle.style.height = `${diameter}px`;
    circle.style.width = `${diameter}px`;
    circle.style.backgroundColor = `rgb(${Math.ceil(Math.random() * 255)}, ${Math.ceil(Math.random() * 255)}, ${Math.ceil(Math.random() * 255)})`;

    return circle
}

function drawCircles(diameter, count) {
    container.style.gridTemplateColumns = `repeat(10, ${+diameter + 10}px)`;
    container.style.gridTemplateRows = `repeat(10, ${+diameter + 10}px)`;

    for (let i = 0; i < count; i++) {
        container.append(generateCircle(diameter))
    }
}

function removeCircles() {
    container.addEventListener('click', (event) => {
        console.log(event.target.className === 'circle');
        if (event.target.className === 'circle'){
            removeCircle(event.target);
        }
    })
}

function removeCircle(circle){
    let op = 1;
    let disappear = setInterval(() => {
        if (op <= 0.1){
            circle.remove();
            clearInterval(disappear)
        }
        circle.style.opacity = `${op}`;
        op -= op * 0.1;
    }, 10)
}


