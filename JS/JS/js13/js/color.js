const wrapper = document.querySelector('.site-wrapper')

if (!localStorage.colorTheme) {
    localStorage.setItem('colorTheme', 'light')
}
if (localStorage.getItem('colorTheme') === 'dark') {
    changeTheme();
}

let toggleButton = document.getElementById('toggle-theme');

toggleButton.addEventListener('click', () => {
    if (localStorage.getItem('colorTheme') === 'light') {
        changeTheme();
        localStorage['colorTheme'] = 'dark';
    } else if (localStorage.getItem('colorTheme') === 'dark') {
        changeTheme();
        localStorage['colorTheme'] = 'light';
    }
});


function changeTheme() {
    wrapper.classList.toggle('dark')
}
