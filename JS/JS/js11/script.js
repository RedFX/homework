const buttons = document.querySelectorAll('.btn');

document.addEventListener('keypress', (event) => {
    console.log(event);
    paintButton(event)
});

function paintButton(event) {
    [...buttons].some(button => {
        console.log(button);
        button.style.backgroundColor = '#33333a';
        if (event.key.toLowerCase() === button.innerText.toLowerCase()) {
            button.style.backgroundColor = 'blue';
            return true;
        }
    })
}
