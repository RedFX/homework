function filterCollection(
    arrayToFilter,
    keywords,
    searchAllMatches,
    ...pathString
) {
    let result = []
    const keywordsArray = keywords.split(" ")
    function searchInArray(arr, keyword) {
        return arr.some(item => {
            if (Array.isArray(item)) {
                return searchInArray(item, keyword)
            } else if (typeof item === "object") {
                return deepSearch(item, keyword)
            } else {
                if (isNaN(item)) return item.toLowerCase() == keyword.toLowerCase()
                return item == keyword
            }
        })
    }
    function deepSearch(obj, keyword) {
        let matchFound = false
        for (let key in obj) {
            if (Array.isArray(obj[key])) {
                matchFound = searchInArray(obj[key], keyword)
            } else if (typeof obj[key] == "object") {
                matchFound = deepSearch(obj[key], keyword)
            } else {
                if (isNaN(obj[key])) {
                    matchFound = obj[key].toLowerCase() == keyword.toLowerCase()
                } else {
                    matchFound = obj[key] == keyword
                }
            }
            if (matchFound) {
                return matchFound
            }
        }
        return matchFound
    }
    function findValueByPath(path, obj) {
        let result = path
            .split(".")
            .reduce((object, property) =>
            {
                console.log("accum - ", object)
                console.log("item - ", property)
                console.log("accum[item] - ", object[property])
                return object[property]}, obj)
        if (isNaN(result)) return result.toLowerCase()
        return result
    }
    if (pathString.length > 0) {
        if (searchAllMatches) {
            result = arrayToFilter.filter(obj =>
                keywordsArray.every(keyword =>
                    pathString.some(
                        path => findValueByPath(path, obj) == keyword.toLowerCase()
                    )
                )
            )
        } else {
            result = arrayToFilter.filter(obj =>
                keywordsArray.some(keyword =>
                    pathString.some(
                        path => findValueByPath(path, obj) == keyword.toLowerCase()
                    )
                )
            )
        }
    } else {
        if (searchAllMatches) {
            result = arrayToFilter.filter(obj => keywordsArray.every(keyword => {
                return deepSearch(obj, keyword)})
            )
        } else {
            result = arrayToFilter.filter(obj =>
                keywordsArray.some(keyword => deepSearch(obj, keyword))
            )
        }
    }
    return result
}
