let vehicles = [
    {
        name: 'Toyota',
        description: 'car',
        contentType: [
            {name: 'collection'},
            {year: 2002},
        ],
        locales: {
            name: 'Тойота',
            description: 'ru_RU'
        }
    },
    {
        name: 'Mercedes',
        description: 'van',
        contentType: [
            {name: 'collection'},
            {year: 2007},
        ],
        locales: {
            name: 'Мерседес',
            description: 'ru_RU'
        }
    }
];


function filterCollection(arrayToFilter, keywords, searchAllMatches, ...fields) {
    console.log(keywords.split(' '));
    keywords = keywords.split(' ');
    fields = fields.map(field => field.split('.'));
    // [['name'], ['description'], ['contentType','name'], ['locales','name'], ['locales','description']]

    function deepSearch(obj, keyword, localFields) {
        Object.entries(obj)
            .filter(([key]) => localFields.some(field => field[0] === key))
            .some(([key, value]) => {
            if(typeof value !== 'object') {
                if (value.toString().toLowerCase() === keyword.toLowerCase()) {
                    console.log(`!!!    Match found! ${value} in ---> ${key}, keyword: ${keyword}  `);
                    return true;
                }
            } else if (Array.isArray(value)) {
                const temp = localFields.filter(field => field[0] === key).map(field => field.slice(1));
                let flag = value.some(elem => {
                    return deepSearch(elem, keyword, temp);
                });
                console.log(`Array flag ${flag}`);
                if (flag) {
                    return flag
                }
            } else if (typeof obj[key] == 'object') {
                return deepSearch(value, keyword, )
            }
        });
        // for (let key in obj) {
        //     console.log(`Now in -> ${key}: ${obj[key]}`);
        //     if (typeof obj[key] !== 'object') {
        //         if (obj[key].toString().toLowerCase() === keyword.toLowerCase()) {
        //             console.log(`!!!    Match found! ${obj[key]} in ---> ${key}, keyword: ${keyword}  `);
        //             return true
        //         }
        //     } else if (Array.isArray(obj[key])) {
        //         let flag = obj[key].some(elem => {
        //             return deepSearch(elem, keyword);
        //         });
        //         console.log(`Array flag ${flag}`);
        //         if (flag) {
        //             return flag
        //         }
        //     } else if (typeof obj[key] == 'object') {
        //         return deepSearch(obj[key], keyword)
        //     }
        // }
    }


    let result = [];
    if (searchAllMatches) {
        result = arrayToFilter.filter(obj => {
            console.log('ENTERED EVERY');
            return keywords.every(keyword => {
                console.log(`Keyword: ${keyword}`);
                return deepSearch(obj, keyword, fields)
            })

        })
    } else {
        result = arrayToFilter.filter(obj => {
            console.log('ENTERED EVERY');
            return keywords.some(keyword => {
                console.log(`Keyword: ${keyword}`);
                return deepSearch(obj, keyword, fields)
            })
        })
    }
    return result
}

const filteredArr = filterCollection(vehicles, 'Toyota', true, name );
console.log(filteredArr)


