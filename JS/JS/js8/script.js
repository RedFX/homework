let formElement = document.querySelector('.form');
let inputElement = document.querySelector('.form-input');
let priceLabel = document.querySelector('.price-label');
console.dir(priceLabel);
let errorElement = document.querySelector('.incorrect-label');
let closeButton = document.querySelector('.span-close');
// let priceValue = ;


inputElement.onfocus = () => {
    inputElement.style.borderColor = `green`;
    inputElement.style.boxShadow = `0px 0px 5px -2px green`;
    console.log(inputElement.style.outlineColor);

    // return to original value, because user want to reenter data
    priceLabel.setAttribute('hidden', `true`);
    errorElement.setAttribute('hidden', `true`);
    inputElement.style.color = `black`;
};

inputElement.onblur = () => {
    validateValue(inputElement.value);
};

closeButton.addEventListener('click', clearPrice)

function validateValue(priceValue) {
    console.log(priceValue);
    if (isNaN(priceValue) || priceValue < 0 || priceValue === ''){
        console.log(`debug`);

        return showError()
    } else {
        return showPrice(priceValue)
    }
}

function showPrice(priceValue) {
    inputElement.style.color = `green`;
    priceLabel.firstChild.data = `Current price: ${priceValue}`;
    priceLabel.removeAttribute('hidden')
}

function showError() {
    inputElement.style.borderColor = `red`;
    inputElement.style.boxShadow = `0px 0px 5px -2px red`;

    errorElement.removeAttribute('hidden');
}

function clearPrice() {
    priceLabel.setAttribute('hidden', 'true');
    inputElement.style.color = `black`;
    inputElement.value = '';
}
