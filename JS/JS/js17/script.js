let n;
let f0 = 0;
let f1 = 1;
do {        //Asking number
    n = prompt('Enter position of wanted Fibonacci number:');

} while (isNaN(n) || n === '' || n === null);

//LOOP REALIZATION

showFibo();

function showFibo() {
    alert(`Fibonacci number on position ${n} is : ${getFibo(f0, f1, +n)}`);
}

function getFibo(f0, f1, n) {
    let fibo = 0;
    let firstInt = f0;
    let secondInt = f1;
    if (n === 0) {
        return f0
    } else if (n === 1 || n === -1) {
        return f1
    } else if (n > 1) {
        let count = 2;
        while (count < n) {
            fibo = firstInt + secondInt;
            firstInt = secondInt;
            secondInt = fibo;
            count++;
            console.log(fibo)
        }
        return fibo;
    } else if (n < -1 ) {
        let count = 0;
        while (count > n + 2) {
            fibo = firstInt - secondInt;
            firstInt = secondInt;
            secondInt = fibo;
            count--;
            console.log(fibo);
            console.log(`count ${count}`)
        }
        return fibo;
    }
}

// RECURSION REALIZATION

// let getFibo = (n) => n < 2 ? n : (getFibo(n - 1) - getFibo(n - 2));
// let showFibo = () => alert (`Fibonacci number on position ${n} is:  ${getFibo(+n)}`);
//
// showFibo();






