let num1;
let num2;

function simpleNum(number) {
    let divider = 2;
    while (number % divider !== 0) {
        divider++;
    }
    return (divider === number);
}
do {
    num1 = +prompt('Enter first number: ');
} while (parseInt(num1) !== num1);
do {
    num2 = +prompt('Enter second number: ');
} while (parseInt(num2) !== num2);
let count = 0;
for (let i = num1; i <= num2; i++) {
    if (i % 5 === 0) {
        console.log(i);
        count++;
    }
}
if (count === 0) {
    console.log('Sorry, no numbers!')
}
for (let i = num1; i <= num2; i++) {
    if (simpleNum(i)) {
        console.log(i + ' simple number.');
    }
}
