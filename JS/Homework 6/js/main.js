function filterString(array, filter) {
    let newArray = [];
    array.forEach((element) => {
        if (typeof element !== filter) {
            if (typeof element === 'object' && element !== null) {
                newArray.push(filterString(element, filter));
            } else {
                newArray.push(element);
            }
        }
    });
    return newArray;
}
console.log('Filter with string', filterString(['hello', 'world', 23, '23', null], 'string'));
