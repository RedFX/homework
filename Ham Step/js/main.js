(function($) {
    $(function() {
        $("ul.tabs__caption").on("click", "li:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("div.tabs__content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})(jQuery);

// var iso = new Isotope( '.grid', {
//     itemSelector: '.element-item',
//     layoutMode: 'fitRows'
// });
//
// // filter functions
// var filterFns = {
//     // show if number is greater than 50
//     numberGreaterThan50: function( itemElem ) {
//         var number = itemElem.querySelector('.number').textContent;
//         return parseInt( number, 10 ) > 50;
//     },
//     // show if name ends with -ium
//     ium: function( itemElem ) {
//         var name = itemElem.querySelector('.name').textContent;
//         return name.match( /ium$/ );
//     }
// };
//
// // bind filter button click
// var filtersElem = document.querySelector('.filters-button-group');
// filtersElem.addEventListener( 'click', function( event ) {
//     // only work with buttons
//     if ( !matchesSelector( event.target, 'button' ) ) {
//         return;
//     }
//     var filterValue = event.target.getAttribute('data-filter');
//     // use matching filter function
//     filterValue = filterFns[ filterValue ] || filterValue;
//     iso.arrange({ filter: filterValue });
// });
//
// // change is-checked class on buttons
// var buttonGroups = document.querySelectorAll('.button-group');
// for ( var i=0, len = buttonGroups.length; i < len; i++ ) {
//     var buttonGroup = buttonGroups[i];
//     radioButtonGroup( buttonGroup );
// }
//
// function radioButtonGroup( buttonGroup ) {
//     buttonGroup.addEventListener( 'click', function( event ) {
//         // only work with buttons
//         if ( !matchesSelector( event.target, 'button' ) ) {
//             return;
//         }
//         buttonGroup.querySelector('.is-checked').classList.remove('is-checked');
//         event.target.classList.add('is-checked');
//     });
// }

$('.button-group').click( function(event){
    $('.is-checked').classList.remove('is-checked');
    event.target.classList.add('is-checked');
});

//filter tabs

$(function() {
    let selectedClass = "";
    $(".filter-button").click(function(){
        selectedClass = $(this).attr("data-rel");
        $(".grid").fadeTo(100, 0.1);
        $(".grid .item").not("."+selectedClass).fadeOut().hide();
        setTimeout(function() {
            $("."+selectedClass).fadeIn().show();
            $(".grid").fadeTo(300, 1);
        }, 300);

    });
});

// modal
$(document).ready(function(){
    $(".menu_search-button").click(function(){
        $(".modal-window").addClass("open-modal");return false;
    });
    $(".close-modal").click(function(){
        $(".modal-window").removeClass("open-modal"); return false;
    });
});

// Load More start

$(document).ready(function() {
    let size_item = $('.loadMore .item').length;
    let i = 12;
    let max_i = 36;
    $('.loadMore .item').slice(0, i).show();
    $('.btn-load-more').click(function () {
        if (i < max_i) {
          i = (i + 12 <= size_item) ? i+12 : size_item;
          $('.loadMore .item').slice(0, i).show();
        };
        i = i;
    });
});

// $(document).ready(function() {
//     let size_item = $('.loadMore .item').length;
//     let i = 12;
//     let max_i = 36;
//     $('.loadMore .item').slice(0, i).show();
//     $('.btn-load-more').click(function () {
//         do {
//             i = (i + 12 <= size_item) ? i+12 : size_item;
//             $('.loadMore .item').slice(0, i).show();
//             return;
//         } while (i < max_i);
//     });
// });


//reviews start
const $slider = $('.review-slider');
const timeoutCount = 2000;
const $reviewName = $('.review-name');
const $reviewPosition = $('.review-position');
const $reviewQuote = $('.review-quote');
const $reviewAvatar = $('.review-avatar');
const sliderContent = [...$('.slider-content:not(:last-child)').children()];
changeData(getData());

let sliderInterval = setInterval(nextSlide, timeoutCount);

$slider.on('click', (e) => {
    clearInterval(sliderInterval);

    $(e.target).is('.slider-avatar') ? toggleSlide(e) :
        $(e.target).is('.btn-next') ? nextSlide() :
            $(e.target).is('.btn-prev') ? prevSlide() : 0;

    sliderInterval = setInterval(nextSlide, timeoutCount);

});

function nextSlide() {
    const $nextSlide = $('.slider-avatar_active').next('.slider-avatar');
    if ($nextSlide.length) {
        $nextSlide
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    } else {
        $slider
            .find('.slider-avatar:first')
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    }
}

function prevSlide() {
    const $prevSlide = $('.slider-avatar_active').prev('.slider-avatar');
    if ($prevSlide.length) {
        $prevSlide
            .siblings()
            .removeClass('slider-avatar_active');
        $prevSlide
            .addClass('slider-avatar_active');
        changeData(getData());
    } else {
        $slider
            .find('.slider-avatar:last')
            .addClass('slider-avatar_active')
            .siblings()
            .removeClass('slider-avatar_active');
        changeData(getData());
    }
}

function toggleSlide(e) {
    $(e.target)
        .addClass('slider-avatar_active')
        .siblings()
        .removeClass('slider-avatar_active');
    changeData(getData())
}

function getData() {
    const $currentSlide = $slider.find('.slider-avatar_active');
    return {
        name: $currentSlide.data('name'),
        position: $currentSlide.data('position'),
        quote: $currentSlide.data('quote'),
        photo: $currentSlide.attr('src')
    }
}

function changeData({name, position, quote, photo}) {
    $reviewName.text(name)
    $reviewPosition.text(position);
    $reviewQuote.text(quote);
    $reviewAvatar
        .fadeTo(50, 0.3, () => {
            $('.review-avatar').attr('src', photo)
        })
        .fadeTo(50, 1)
}


const trackChange = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
        const trackedNode = $(mutation.target);
        if (mutation.removedNodes.length) {
            trackedNode.fadeOut(1)
        }
        if (mutation.addedNodes.length) {
            trackedNode.fadeIn(100)
        }
    })
});


window.addEventListener('scroll', () => {
    if (isInViewPort($('.slider-content')[0])) {
        sliderContent.forEach(node => {
            trackChange.observe(node, {
                characterData: false,
                attributes: false,
                childList: true,
                subtree: false
            });
        })
    } else trackChange.disconnect()
});


//gallery best

